#  LICENSE

This software is provided solely for the purpose of evaluating the Modelar Scan iOS SDK. It must not be redistributed or used in any derivative works without prior permission from Modelar Technologies. 

Copyright Modelar Technologies 2022.
