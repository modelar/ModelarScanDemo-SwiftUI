//
//  ScanView.swift
//  Demo-SwiftUI
//
//  Copyright Modelar Technologies 2022.
//

import SwiftUI
import ModelarScan

struct ScanView: UIViewControllerRepresentable {
    typealias UIViewControllerType = ScanController

    func makeUIViewController(context: Context) -> ScanController {
        
        // Authenticate the license (network connection required)
        // This only needs to be done once when the app starts
        Authenticator.shared.authenticate(licenseKey: "LICENSE-KEY")
        
        // Create a scan controller
        let scanController = ScanController()

        // Configure real-time scanning
        scanController.scanSensorDensity = .high
        scanController.scanResolution = 0.05
        scanController.scanBackgroundTransparency = 0.3
        scanController.isPauseHidden = true
        
        // Configure automatic post-scanning refinement
        scanController.refinementQuality = .high
        
        // Configure model export settings
        scanController.upAxis = .z
        scanController.scanOrigin = .center
        
        // Add a handler that's called when a scan is accepted
        scanController.scanAcceptedHandler = { (acceptedScanPath: URL, acceptedSnapshot: UIImage) in
            
            // Print the path of the USDZ file
            print(acceptedScanPath)
            
            // Create a controller to share or save the file
            let objectsToShare = [acceptedScanPath]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            // Present the activity controller over the scan controller
            activityVC.modalPresentationStyle = .automatic
            
            // Supply a view and frame for activity presentation controller to avoid
            // issues on iPads
            if let popOver = activityVC.popoverPresentationController {
                popOver.sourceView = scanController.view
                popOver.sourceRect = CGRect(x: scanController.view.bounds.width/2, y: scanController.view.bounds.height, width: 0, height: 0)
                popOver.permittedArrowDirections = .down
            }
            
            scanController.present(activityVC, animated:true)
        }
        
        // return the scan controller
        return scanController
    }
    
    func updateUIViewController(_ uiViewController: ScanController, context: Context) {
        // no operations
    }
}

struct ScanView_Previews: PreviewProvider {
    static var previews: some View {
        ScanView()
.previewInterfaceOrientation(.portrait)
    }
}

