//
//  Demo_SwiftUIApp.swift
//  Demo-SwiftUI
//
//  Copyright Modelar Technologies 2022.
//

import SwiftUI
import ModelarScan

@main
struct Demo_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            // The scan view must cover the full screen
            ScanView()
                .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
